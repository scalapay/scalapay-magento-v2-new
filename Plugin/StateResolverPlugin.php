<?php
/*
 * Copyright © Scalapay S.R.L. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Plugin;

use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\OrderStateResolverInterface;
use Magento\Sales\Model\Order\StateResolver;
use Scalapay\Scalapay\Gateway\Settings;

class StateResolverPlugin
{
    /**
     * @var Settings
     */
    private $settings;

    /**
     * StateResolverPlugin constructor.
     * @param Settings $settings
     */
    public function __construct(
        Settings $settings
    ) {
        $this->settings = $settings;
    }

    /**
     * @param StateResolver $subject
     * @param $result
     * @param OrderInterface $order
     * @param array $arguments
     * @return mixed|string
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetStateForOrder(
        StateResolver $subject,
        $result,
        OrderInterface $order,
        array $arguments = []
    ) {
        if ($this->isOrderProcessing($order, $arguments) &&
            $order->getPayment()->getMethod() === $this->settings->getCode()) {
            $result = $this->settings->getOrderStatus();
        }

        return $result;
    }

    /**
     * @param OrderInterface $order
     * @param $arguments
     * @return bool
     */
    private function isOrderProcessing(OrderInterface $order, $arguments): bool
    {
        /** @var $order Order|OrderInterface */
        if ($order->getState() === Order::STATE_NEW &&
            in_array(OrderStateResolverInterface::IN_PROGRESS, $arguments)) {
            return true;
        }
        return false;
    }
}
