<?php
/*
 * Copyright © Scalapay S.R.L. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Test\Integration;

use Magento\Framework\UrlInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\TestFramework\Helper\Bootstrap;
use Scalapay\Scalapay\Gateway\Settings;
use PHPUnit\Framework\TestCase;
use ReflectionClass;

class SettingsTest extends TestCase
{
    /**
     * @var Settings
     */
    private $subject;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @return void
     */
    public function setUp(): void
    {
        $objectManager = Bootstrap::getObjectManager();
        $this->subject = $objectManager->get(Settings::class);
        $this->storeManager = $objectManager->get(StoreManagerInterface::class);
    }

    /**
     * @return void
     * @throws \ReflectionException
     */
    public function testShouldVerifySettingsNumber(): void
    {
        $reflectionClass = new ReflectionClass($this->subject);
        $constants = $reflectionClass->getConstants();
        $this->assertCount(22, $constants);
    }

    /**
     * @return void
     * @magentoConfigFixture current_store payment/scalapay/active 1
     * @magentoConfigFixture current_store payment/scalapay/disabled_categories 1,2
     * @magentoConfigFixture current_store payment/scalapay/allow_specific_countries 1
     * @magentoConfigFixture current_store payment/scalapay/specific_countries DE,IT
     * @magentoConfigFixture current_store payment/scalapay/allow_specific_currencies 1
     * @magentoConfigFixture current_store payment/scalapay/specific_currencies EUR,USD
     * @magentoConfigFixture current_store payment/scalapay/allow_specific_languages 1
     * @magentoConfigFixture current_store payment/scalapay/specific_languages de_DE,it_IT
     * @magentoConfigFixture current_store payment/scalapay/sort_order 100
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function testShouldVerifyBaseSettingsValues(): void
    {
        $this->assertSame('scalapay', $this->subject->getCode());
        $this->assertTrue($this->subject->isActive());
        $this->assertSame('Scalapay', $this->subject->getTitle());
        $this->assertFalse($this->subject->getLiveMode());
        $this->assertSame('https://staging.api.scalapay.com', $this->subject->getTestUrl());
        $this->assertSame('qhtfs87hjnc12kkos', $this->subject->getTestApiKey());
        $this->assertSame('https://api.scalapay.com', $this->subject->getProductionUrl());
        $this->assertNull($this->subject->getProductionApiKey());
        $this->assertSame(
            $this->storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_WEB),
            $this->subject->getSiteUrl()
        );
        $this->assertSame('processing', $this->subject->getOrderStatus());
        $this->assertSame(300.0, $this->subject->getMaxAmount());
        $this->assertSame(5.0, $this->subject->getMinAmount());
        $this->assertSame(3, $this->subject->getNumberOfPayments());
        $this->assertSame([1,2], $this->subject->getDisabledCategories());
        $this->assertTrue($this->subject->getAllowSpecificCountries());
        $this->assertSame(['DE','IT'], $this->subject->getSpecificCountries());
        $this->assertTrue($this->subject->getAllowSpecificCurrencies());
        $this->assertSame(['EUR','USD'], $this->subject->getSpecificCurrencies());
        $this->assertTrue($this->subject->getAllowSpecificLanguages());
        $this->assertSame(['de_DE','it_IT'], $this->subject->getSpecificLanguages());
        $this->assertSame(100, $this->subject->getSortOrder());
    }
}
