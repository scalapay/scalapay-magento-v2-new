<?php
/*
 * Copyright © Scalapay S.R.L. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Test\Integration;

use Magento\TestFramework\Helper\Bootstrap;
use Scalapay\Scalapay\Model\IsCurrencyAllowed;
use PHPUnit\Framework\TestCase;

class IsCurrencyAllowedTest extends TestCase
{
    /**
     * @var IsCurrencyAllowed
     */
    private $subject;

    /**
     * @return void
     */
    public function setUp(): void
    {
        $om = Bootstrap::getObjectManager();
        $this->subject = $om->get(IsCurrencyAllowed::class);
    }

    /**
     * @return array
     */
    public function shouldCheckAllowedCurrencyDataProvider(): array
    {
        return [
            ['it', true],
            ['fr', true],
            ['de', true],
            ['jp', false],
            ['', true],
        ];
    }

    /**
     * @param string $currency
     * @param bool $expected
     * @return void
     * @dataProvider shouldCheckAllowedCurrencyDataProvider
     * @magentoConfigFixture default_store payment/scalapay/specific_currencies it,fr,de
     * @magentoConfigFixture default_store payment/scalapay/allow_specific_currencies 1
     */
    public function testShouldCheckAllowedCurrency(string $currency, bool $expected): void
    {
        $this->assertSame($expected, $this->subject->execute($currency));
    }

    /**
     * @return array
     */
    public function shouldCheckAllowedCurrencyWithNoSpecificCurrenciesDataProvider(): array
    {
        return [
            ['it', true],
            ['fr', true],
            ['de', true],
            ['jp', true],
            ['', true],
        ];
    }

    /**
     * @param string $currency
     * @param bool $expected
     * @return void
     * @dataProvider shouldCheckAllowedCurrencyWithNoSpecificCurrenciesDataProvider
     * @magentoConfigFixture default_store payment/scalapay/specific_currencies it,fr,de
     * @magentoConfigFixture default_store payment/scalapay/allow_specific_currencies 0
     */
    public function testShouldCheckAllowedCurrencyWithNoSpecificCurrencies(string $currency, bool $expected): void
    {
        $this->assertSame($expected, $this->subject->execute($currency));
    }
}
