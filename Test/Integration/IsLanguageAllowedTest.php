<?php
/*
 * Copyright © Scalapay S.R.L. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Test\Integration;

use Magento\TestFramework\Helper\Bootstrap;
use Scalapay\Scalapay\Model\IsLanguageAllowed;
use PHPUnit\Framework\TestCase;

class IsLanguageAllowedTest extends TestCase
{
    /**
     * @var IsLanguageAllowed
     */
    private $subject;

    /**
     * @return void
     */
    public function setUp(): void
    {
        $om = Bootstrap::getObjectManager();
        $this->subject = $om->get(IsLanguageAllowed::class);
    }

    /**
     * @return array
     */
    public function shouldCheckAllowedLanguageDataProvider(): array
    {
        return [
            ['it', true],
            ['fr', true],
            ['de', true],
            ['jp', false],
            ['', true],
        ];
    }

    /**
     * @param string $language
     * @param bool $expected
     * @return void
     * @dataProvider shouldCheckAllowedLanguageDataProvider
     * @magentoConfigFixture default_store payment/scalapay/specific_languages it,fr,de
     * @magentoConfigFixture default_store payment/scalapay/allow_specific_languages 1
     */
    public function testShouldCheckAllowedLanguage(string $language, bool $expected): void
    {
        $this->assertSame($expected, $this->subject->execute($language));
    }

    /**
     * @return array
     */
    public function shouldCheckAllowedLanguageWithNoSpecificLanguagesDataProvider(): array
    {
        return [
            ['it', true],
            ['fr', true],
            ['de', true],
            ['jp', true],
            ['', true],
        ];
    }

    /**
     * @param string $language
     * @param bool $expected
     * @return void
     * @dataProvider shouldCheckAllowedLanguageWithNoSpecificLanguagesDataProvider
     * @magentoConfigFixture default_store payment/scalapay/specific_languages it,fr,de
     * @magentoConfigFixture default_store payment/scalapay/allow_specific_languages 0
     */
    public function testShouldCheckAllowedLanguageWithNoSpecificLanguages(string $language, bool $expected): void
    {
        $this->assertSame($expected, $this->subject->execute($language));
    }
}
