<?php
/*
 * Copyright © Scalapay S.R.L. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Test\Integration;

use Magento\TestFramework\Helper\Bootstrap;
use Scalapay\Scalapay\Model\IsCountryAllowed;
use PHPUnit\Framework\TestCase;

class IsCountryAllowedTest extends TestCase
{
    /**
     * @var IsCountryAllowed
     */
    private $subject;

    /**
     * @return void
     */
    public function setUp(): void
    {
        $om = Bootstrap::getObjectManager();
        $this->subject = $om->get(IsCountryAllowed::class);
    }

    /**
     * @return array
     */
    public function shouldCheckAllowedCountryDataProvider(): array
    {
        return [
            ['it', true],
            ['fr', true],
            ['de', true],
            ['jp', false],
            ['', true],
        ];
    }

    /**
     * @param string $country
     * @param bool $expected
     * @return void
     * @dataProvider shouldCheckAllowedCountryDataProvider
     * @magentoConfigFixture default_store payment/scalapay/specific_countries it,fr,de
     * @magentoConfigFixture default_store payment/scalapay/allow_specific_countries 1
     */
    public function testShouldCheckAllowedCountry(string $country, bool $expected): void
    {
        $this->assertSame($expected, $this->subject->execute($country));
    }

    /**
     * @return array
     */
    public function shouldCheckAllowedCountryWithNoSpecificCountriesDataProvider(): array
    {
        return [
            ['it', true],
            ['fr', true],
            ['de', true],
            ['jp', true],
            ['', true],
        ];
    }

    /**
     * @param string $country
     * @param bool $expected
     * @return void
     * @dataProvider shouldCheckAllowedCountryWithNoSpecificCountriesDataProvider
     * @magentoConfigFixture default_store payment/scalapay/specific_countries it,fr,de
     * @magentoConfigFixture default_store payment/scalapay/allow_specific_countries 0
     */
    public function testShouldCheckAllowedCountryWithNoSpecificCountries(string $country, bool $expected): void
    {
        $this->assertSame($expected, $this->subject->execute($country));
    }
}
