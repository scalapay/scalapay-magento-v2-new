<?php
/*
 * Copyright © Scalapay S.R.L. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Gateway;

use Magento\Payment\Gateway\Config\Config as GatewayConfig;

/**
 * Gateway config
 */
class SettingsPayin4 extends GatewayConfig
{
    const CODE = 'payin4';
    const ACTIVE = 'active';
    const TITLE = 'title';
    const MODE = 'mode';
    const TEST_URL = 'test_url';
    const TEST_API_KEY = 'test_api_key';
    const PRODUCTION_URL = 'production_url';
    const PRODUCTION_API_KEY = 'production_api_key';
    const SITE_URL = 'site_url';
    const ORDER_STATUS = 'order_status';
    const DELAYED_CAPTURE = 'delayed_capture';
    const INSTRUCTIONS = 'instructions';
    const MAX_AMOUNT = 'max_amount';
    const MIN_AMOUNT = 'min_amount';
    const DISABLED_CATEGORIES = 'disabled_categories';
    const DISABLED_PRODUCT_TYPES = 'disabled_product_types';
    const ALLOW_SPECIFIC_COUNTRIES = 'allow_specific_countries';
    const SPECIFIC_COUNTRIES = 'specific_countries';
    const ALLOW_SPECIFIC_CURRENCIES = 'allow_specific_currencies';
    const SPECIFIC_CURRENCIES = 'specific_currencies';
    const ALLOW_SPECIFIC_LANGUAGES = 'allow_specific_languages';
    const SPECIFIC_LANGUAGES = 'specific_languages';

    /**
     * @return string
     */
    public function getCode(): string
    {
        return self::CODE;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return (bool) $this->getValue(self::ACTIVE);
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->getValue(self::TITLE);
    }

    /**
     * @return bool
     */
    public function getLiveMode(): bool
    {
        return (bool) $this->getValue(self::MODE);
    }

    /**
     * @return string
     */
    public function getTestUrl(): string
    {
        return $this->getValue(self::TEST_URL);
    }

    /**
     * @return string
     */
    public function getTestApiKey(): string
    {
        return $this->getValue(self::TEST_API_KEY);
    }

    /**
     * @return string
     */
    public function getProductionUrl(): string
    {
        return $this->getValue(self::PRODUCTION_URL);
    }

    /**
     * @return string|null
     */
    public function getProductionApiKey(): ?string
    {
        return $this->getValue(self::PRODUCTION_API_KEY);
    }

    /**
     * @return string
     */
    public function getSiteUrl(): string
    {
        return $this->getValue(self::SITE_URL);
    }

    /**
     * @return string
     */
    public function getOrderStatus(): string
    {
        return $this->getValue(self::ORDER_STATUS);
    }

    /**
     * @return bool
     */
    public function getDelayedCapture(): bool
    {
        return (bool) $this->getValue(self::DELAYED_CAPTURE);
    }

    /**
     * @return string
     */
    public function getInstructions(): string
    {
        return $this->getValue(self::INSTRUCTIONS);
    }

    /**
     * @return float
     */
    public function getMaxAmount(): float
    {
        return (float) $this->getValue(self::MAX_AMOUNT);
    }

    /**
     * @return float
     */
    public function getMinAmount(): float
    {
        return (float) $this->getValue(self::MIN_AMOUNT);
    }

    /**
     * @return int
     */
    public function getNumberOfPayments(): int
    {
        return 4;
    }

    /**
     * @return int[]
     */
    public function getDisabledCategories(): array
    {
        $s = (string) $this->getValue(self::DISABLED_CATEGORIES);
        if (!$s) {
            return [];
        }

        return array_map(static function ($item) { return (int) $item; }, explode(',', $s));
    }

    /**
     * @return string[]
     */
    public function getDisabledProductTypes(): array
    {
        $s = (string) $this->getValue(self::DISABLED_PRODUCT_TYPES);
        if (!$s) {
            return [];
        }

        return array_map(static function ($item) { return (string) $item; }, explode(',', $s));
    }

    /**
     * @return bool
     */
    public function getAllowSpecificCountries(): bool
    {
        return (bool) $this->getValue(self::ALLOW_SPECIFIC_COUNTRIES);
    }

    /**
     * @return string[]
     */
    public function getSpecificCountries(): array
    {
        if ($this->getValue(self::SPECIFIC_COUNTRIES) !== null) {
            return explode(',', $this->getValue(self::SPECIFIC_COUNTRIES));
        }
        return [];
    }

    /**
     * @return bool
     */
    public function getAllowSpecificCurrencies(): bool
    {
        return (bool) $this->getValue(self::ALLOW_SPECIFIC_CURRENCIES);
    }

    /**
     * @return string[]
     */
    public function getSpecificCurrencies(): array
    {
        if ($this->getValue(self::SPECIFIC_CURRENCIES) !== null) {
            return explode(',', $this->getValue(self::SPECIFIC_CURRENCIES));
        }
        return [];
    }

    /**
     * @return bool
     */
    public function getAllowSpecificLanguages(): bool
    {
        return (bool) $this->getValue(self::ALLOW_SPECIFIC_LANGUAGES);
    }

    /**
     * @return string[]
     */
    public function getSpecificLanguages(): array
    {
        if ($this->getValue(self::SPECIFIC_LANGUAGES) !== null) {
            return explode(',', $this->getValue(self::SPECIFIC_LANGUAGES));
        }
        return [];
    }
}
