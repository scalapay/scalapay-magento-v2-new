<?php
/*
 * Copyright © Scalapay S.R.L. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Gateway\Request;

use Magento\Payment\Gateway\Request\BuilderInterface;

class DelayedCaptureRequest implements BuilderInterface
{
    /**
     * @inheritDoc
     */
    public function build(array $buildSubject): array
    {
        return [
            'payment' => $buildSubject['payment']->getPayment()
        ];
    }
}
