<?php

namespace Scalapay\Scalapay\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{
    protected $productMetadataInterface;
    protected $datetime;
    protected $scalapayConfigV3;
    protected $jsonHelper;
    protected $settings;

    public function __construct(
        \Magento\Framework\App\ProductMetadataInterface $productMetadataInterface,
        \Magento\Framework\Stdlib\DateTime\DateTime $datetime,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Scalapay\Scalapay\Gateway\Settings $settings
    ) {
        $this->productMetadataInterface = $productMetadataInterface;
        $this->datetime = $datetime;
        $this->jsonHelper = $jsonHelper;
        $this->settings = $settings;
    }

    public function log($log_string)
    {
        if (isset($log_string)) {
            $oldFileName = $this->getLogFileName("-2 months");

            if (file_exists($oldFileName)) {
                unlink($oldFileName);
            }

            $filename = $this->getLogFileName();
            try {
                if (version_compare($this->productMetadataInterface->getVersion(), '2.4.2', '<=')) {
                    //version is 2.4.2 or below
                    $writer = new \Zend\Log\Writer\Stream($filename);
                    $logger = new \Zend\Log\Logger();
                    $logger->addWriter($writer);

                    if (!is_array($log_string) && !is_object($log_string)) {
                        $logger->info($log_string);
                    } else {
                        $logger->info(print_r($log_string, true));
                    }
                } else {
                    //version is higher 2.4.2
                    $writer = new \Zend_Log_Writer_Stream($filename);
                    $logger = new \Zend_Log();
                    $logger->addWriter($writer);

                    if (!is_array($log_string) && !is_object($log_string)) {
                        $logger->info($log_string);
                    } else {
                        $logger->info(print_r($log_string, true));
                    }
                }
            } catch (\Exception $e) {
                $e->getMessage();
            }
        }
    }

    public function getLogFileName($fileDate = "")
    {
        $basePath = \Magento\Framework\App\Filesystem\DirectoryList::getDefaultConfig();
        $filename = "";
        $logDir = BP . "/" . $basePath['log']['path'];

        if (isset($fileDate) && $fileDate !== "") {
            $filename = $logDir . '/scalapay_' . $this->datetime->date('Y_m', $fileDate) . '.log';
        } else {
            $filename = $logDir . '/scalapay_' . $this->datetime->date('Y_m') . '.log';
        }

        return $filename;
    }

    protected function getCurlHandler($urlKey, $data = "")
    {
        $response = "";
        $baseUrl = "";
        $secretKey = "";
        if ($this->settings->getLiveMode()) {
            $baseUrl = $this->settings->getProductionUrl();
            $secretKey = $this->settings->getProductionApiKey();
        } else {
            $baseUrl = $this->settings->getTestUrl();
            $secretKey = $this->settings->getTestApiKey();
        }

        if ($urlKey != "") {
            try {
                $url = $baseUrl . "/" . $urlKey;
                $curl = curl_init();
                $curlOption = array(
                  CURLOPT_URL => $url,
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_ENCODING => '',
                  CURLOPT_MAXREDIRS => 10,
                  CURLOPT_TIMEOUT => 0,
                  CURLOPT_FOLLOWLOCATION => true,
                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                  CURLOPT_HTTPHEADER => array(
                    'Accept: application/json',
                    'Authorization: Bearer ' . $secretKey,
                    'Content-Type: application/json'
                  ),
                );
                if (isset($data) && $data != "") {
                    $curlOption[CURLOPT_POSTFIELDS] = $data;
                }
                curl_setopt_array($curl, $curlOption);
                $response = curl_exec($curl);
                $this->log($response);
                curl_close($curl);
            } catch (\Exception $e) {
                $response = $this->_jsonHelper->jsonEncode(['error' => $e->getMessage()]);
                $this->log($e->getMessage());
            }
        }

        return $response;
    }

    /**
     * get Scalapay V3 configurations
     *
     * @return Array
     *
     */
    public function getScalapayConfigV3()
    {
        $this->log("calling get Scalapay config V3.");
        if (!isset($this->scalapayConfigV3)) {
            $this->log("getting Scalapay config V3 values.");
            $urlKey = "v3/configurations";
            $response = $this->getCurlHandler($urlKey);
            if ($response != "Unauthorized") {
                $ins = $this->jsonHelper->jsonDecode($response);
                if (isset($ins)) {
                    foreach ($ins as $res) {
                        if ($res["product"] == 'pay-in-3' && $res["type"] == 'online') {
                            if (isset($res["configuration"]["minimumAmount"]["amount"])) {
                                $this->scalapayConfigV3['payin3']["minimumAmount"] = $res["configuration"]["minimumAmount"]["amount"];
                            }
                            if (isset($res["configuration"]["maximumAmount"]["amount"])) {
                                $this->scalapayConfigV3['payin3']["maximumAmount"] = $res["configuration"]["maximumAmount"]["amount"];
                            }
                            $this->scalapayConfigV3['payin3']["numberOfPayments"] = 3;
                        }
                        if ($res["product"] == 'pay-in-4' && $res["type"] == 'online') {
                            if (isset($res["configuration"]["minimumAmount"]["amount"])) {
                                $this->scalapayConfigV3['payin4']["minimumAmount"] = $res["configuration"]["minimumAmount"]["amount"];
                            }
                            if (isset($res["configuration"]["maximumAmount"]["amount"])) {
                                $this->scalapayConfigV3['payin4']["maximumAmount"] = $res["configuration"]["maximumAmount"]["amount"];
                            }
                            $this->scalapayConfigV3['payin4']["numberOfPayments"] = 4;
                        }
                        if ($res["product"] == 'later' && $res["type"] == 'online') {
                            if (isset($res["configuration"]["minimumAmount"]["amount"])) {
                                $this->scalapayConfigV3['paylater']["minimumAmount"] = $res["configuration"]["minimumAmount"]["amount"];
                            }
                            if (isset($res["configuration"]["maximumAmount"]["amount"])) {
                                $this->scalapayConfigV3['paylater']["maximumAmount"] = $res["configuration"]["maximumAmount"]["amount"];                            }
                            $this->scalapayConfigV3['paylater']["numberOfPayments"] = 1;
                            $this->scalapayConfigV3['paylater']["number"] = $res["frequency"]["number"];
                        }
                    }
                }
            }
        }

        return $this->scalapayConfigV3;
    }
}
