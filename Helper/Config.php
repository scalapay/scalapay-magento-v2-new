<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Config
 *
 * @author Sclapay Plugin Integration Team
 * @package Scalapay\Scalapay\Helper
 */
class Config extends AbstractHelper
{
    /** @var IN_PAGE_CHECKOUT_CDN_JS */
    const IN_PAGE_CHECKOUT_CDN_JS = 'https://cdn.scalapay.com/in-page-checkout/popup.min.js';

    /** @var IN_PAGE_CHECKOUT_CDN_HTML */
    const IN_PAGE_CHECKOUT_CDN_HTML = 'https://cdn.scalapay.com/in-page-checkout/popup.html';

    /**
     * Retrieve a configuration by a given configuration key.
     *
     * @param string $configKey
     * @param string $scope
     * @return string
     */
    public function getConfig(string $configKey, string $scope = ScopeInterface::SCOPE_STORES): string
    {
        return (string) $this->scopeConfig->getValue($configKey, $scope);
    }

    /**
     * Returns if the in page checkout mode is enabled.
     *
     * @return bool
     */
    public function inPageCheckoutIsEnabled(): bool
    {
        return (int) $this->getConfig('scalapay/inpagecheckout/enabled') === 1;
    }

    /**
     * Retrieve a payment configuration by a given field.
     *
     * @param $field
     * @return string
     */
    public function getPaymentConfig($field): string
    {
        return (string) $this->getConfig('payment/' . $field);
    }

    /**
     * Retrieve a tax configuration by a given field.
     *
     * @param $field
     * @return int
     */
    public function getTaxConfig($field): int
    {
        return (int) $this->getConfig('tax/' . $field);
    }

    /**
     * Returns true if a payment is enabled else false.
     *
     * @param $field
     * @return bool
     */
    public function isPaymentEnabled($field): bool
    {
        return (int) $this->getPaymentConfig($field) === 1;
    }
}
