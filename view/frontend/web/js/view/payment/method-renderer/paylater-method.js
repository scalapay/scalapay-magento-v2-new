/*
 * Copyright © Scalapay S.R.L. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'Magento_Checkout/js/view/payment/default',
    'ko',
    'mage/url',
    'Magento_Checkout/js/model/quote',
    'Magento_Catalog/js/price-utils'
], function (Component, ko, url, quote, priceUtils) {
    'use strict';

    return Component.extend({
        redirectAfterPlaceOrder: false,
        defaults: {
            template: 'Scalapay_Scalapay/payment/paylater'
        },

        title: ko.observable(""),

        updateTitle: function () {
            this.title(this.getScalapayTitle());
        },

        initialize: function () {
            this._super();

            quote.totals.subscribe(this.updateTitle.bind(this));
            this.updateTitle(this);
        },

        afterPlaceOrder: function () {
            window.location.replace(url.build('scalapay/redirect/index'));
        },

        getScalapayTitle: function () {
            const config = window.checkoutConfig.payment.paylater;
            const str = config.title;
            const logo = config.logo;
            return str
                .replace("[logo]", logo);
        },

        getInstructions: function () {
            const config = window.checkoutConfig.payment.paylater;
            const str = config.instructions;

            return str;
        },
    });
});
