<?php
/*
 * Copyright © Scalapay S.R.L. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Ui\ViewModel;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Locale\Resolver;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Store\Model\StoreManagerInterface;
use Scalapay\Scalapay\Gateway\Settings;
use Scalapay\Scalapay\Helper\Config as ConfigHelper;
use Scalapay\Scalapay\Model\IsCountryAllowed;
use Scalapay\Scalapay\Model\IsCurrencyAllowed;
use Scalapay\Scalapay\Model\IsLanguageAllowed;
use Scalapay\Scalapay\Model\ResourceModel\IsProductListInDisabledCategories;
use Scalapay\Scalapay\Model\ResourceModel\PayIn3\IsProductListInDisabledProductTypes;

/**
 * Scalapay widget view model
 */
class ScalaWidgetConfig implements ArgumentInterface
{
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * @var string
     */
    private $type;
    /**
     * @var ConfigHelper
     */
    private $configHelper;
    /**
     * @var Settings
     */
    private $settings;
    /**
     * @var Resolver
     */
    private $localeResolver;
    /**
     * @var IsProductListInDisabledCategories
     */
    private $isProductListInDisabledCategories;
    /**
     * @var IsProductListInDisabledProductTypes
     */
    private $isProductListInDisabledProductTypes;
    /**
     * @var IsCountryAllowed
     */
    private $isCountryAllowed;
    /**
     * @var IsLanguageAllowed
     */
    private $isLanguageAllowed;
    /**
     * @var IsCurrencyAllowed
     */
    private $isCurrencyAllowed;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @param IsProductListInDisabledCategories $isProductListInDisabledCategories
     * @param IsProductListInDisabledProductTypes $isProductListInDisabledProductTypes
     * @param StoreManagerInterface $storeManager
     * @param IsCountryAllowed $isCountryAllowed
     * @param IsLanguageAllowed $isLanguageAllowed
     * @param IsCurrencyAllowed $isCurrencyAllowed
     * @param ScopeConfigInterface $scopeConfig
     * @param Resolver $localeResolver
     * @param ConfigHelper $configHelper
     * @param Settings $settings
     * @param SerializerInterface $serializer
     * @param string $type
     */
    public function __construct(
        IsProductListInDisabledCategories $isProductListInDisabledCategories,
        IsProductListInDisabledProductTypes $isProductListInDisabledProductTypes,
        StoreManagerInterface $storeManager,
        IsCountryAllowed $isCountryAllowed,
        IsLanguageAllowed $isLanguageAllowed,
        IsCurrencyAllowed $isCurrencyAllowed,
        ScopeConfigInterface $scopeConfig,
        Resolver $localeResolver,
        ConfigHelper $configHelper,
        Settings $settings,
        SerializerInterface $serializer,
        string $type
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->type = $type;
        $this->configHelper = $configHelper;
        $this->settings = $settings;
        $this->localeResolver = $localeResolver;
        $this->isProductListInDisabledCategories = $isProductListInDisabledCategories;
        $this->isProductListInDisabledProductTypes = $isProductListInDisabledProductTypes;
        $this->isCountryAllowed = $isCountryAllowed;
        $this->isLanguageAllowed = $isLanguageAllowed;
        $this->isCurrencyAllowed = $isCurrencyAllowed;
        $this->storeManager = $storeManager;
        $this->serializer = $serializer;
    }

    /**
     * @param string $path
     * @return string
     */
    private function getConfig(string $path): string
    {
        return (string) $this->configHelper->getConfig('scalapay/payin3/' . $this->type . '/' . $path);
    }

    /**
     * @return bool
     */
    public function isVisible(): bool
    {
        return (bool) $this->getConfig('show_widget');
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return (string) $this->type;
    }

    /**
     * @return string
     */
    public function getBelowWidgetText(): string
    {
        if (
            $this->type == 'product' &&
            $this->getConfig('scalapay_enable_below_widget_text') &&
            $this->getConfig('scalapay_below_widget_text')
        ) {
            //return (string) $this->getConfig('scalapay_below_widget_text') . ' ' . $this->getMinAmount();
            return (string) $this->getConfig('scalapay_below_widget_text');
        }

        return '';
    }

    /**
     * @return bool
     */
    public function shouldApplyMinOrderValue(): bool
    {
        return (bool) $this->getConfig('apply_min_order_value');
    }

    /**
     * @return float
     */
    public function getMinAmount(): float
    {
        if (!$this->shouldApplyMinOrderValue()) {
            return 0.01;
        }

        return $this->settings->getMinAmount();
    }

    /**
     * @return float
     */
    public function getMaxAmount(): float
    {
        return $this->settings->getMaxAmount();
    }

    /**
     * @return int
     */
    public function getNumberOfPayments(): int
    {
        return $this->settings->getNumberOfPayments();
    }

    /**
     * @return string
     */
    public function getTextSize(): string
    {
        return $this->getConfig('text_size');
    }

    /**
     * @return string
     */
    public function getLogoSize(): string
    {
        return $this->getConfig('logo_size');
    }

    /**
     * @return string
     */
    public function getPriceColor(): string
    {
        return $this->getConfig('price_color');
    }

    /**
     * @return string
     */
    public function getLogoColor(): string
    {
        return $this->getConfig('logo_color');
    }

    /**
     * @return string
     */
    public function getCurrencyPosition(): string
    {
        return $this->getConfig('currency_position');
    }

    /**
     * @return string
     */
    public function getCurrencyDisplay(): string
    {
        return $this->getConfig('currency_display');
    }

     /**
     * @return string
     */
    public function getTheme(): string
    {
        return $this->getConfig('theme');
    }

    /**
     * @return string
     */
    public function getLogoAlignment(): string
    {
        return $this->getConfig('logo_alignment');
    }

    /**
     * @return string
     */
    public function isLogoShown(): string
    {
        return $this->getConfig('show_logo') ? 'true': 'false';
    }

    /**
     * @return string
     */
    public function isPriceHidden(): string
    {
        return $this->getConfig('hide_price') ? 'true': 'false';
    }

    /**
     * @return string
     */
    public function isTitleShown(): string
    {
        return $this->getConfig('show_title') ? 'true': 'false';
    }

    /**
     * @return string
     */
    public function getAmountSelectors(): string
    {
        $selectorsArray = preg_split('/[\n\r]+/', $this->getConfig('amount_selector_array'));
        return $this->serializer->serialize($selectorsArray);
    }

    /**
     * @return string
     */
    public function getPriceBoxSelector(): string
    {
        $selectorsArray = preg_split('/[\n\r]+/', $this->getConfig('pricebox_selector'));
        return $this->serializer->serialize($selectorsArray);
    }

    /**
     * @return string
     */
    public function getTextPosition(): string
    {
        return $this->getConfig('text_position');
    }

    /**
     * @return string
     */
    public function getCurrentLanguage(): string
    {
        $locale = $this->localeResolver->getLocale();
        return substr($locale, 0, 2);
    }

    /**
     * @param float $amount
     * @param array $productsIds
     * @param string $currentCountry
     * @return bool
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function isScalapayActive(float $amount, array $productsIds = [], string $currentCountry = ""): bool
    {
        if (!$this->settings->isActive()) {
            return false;
        }

        if (($amount < $this->getMinAmount()) || ($amount > $this->getMaxAmount())) {
            return false;
        }

        if ($this->isProductListInDisabledCategories->execute($productsIds)) {
            return false;
        }

        if ($this->isProductListInDisabledProductTypes->execute($productsIds)) {
            return false;
        }

        if (!$this->isCountryAllowed->execute($currentCountry)) {
            return false;
        }

        $currentLanguage = $this->localeResolver->getLocale();
        if (!$this->isLanguageAllowed->execute($currentLanguage)) {
            return false;
        }

        $currentCurrency = $this->storeManager->getStore()->getCurrentCurrency()->getCode();
        if (!$this->isCurrencyAllowed->execute($currentCurrency)) {
            return false;
        }

        return true;
    }
}
