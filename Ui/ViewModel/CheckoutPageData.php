<?php
/*
 * Copyright © Scalapay S.R.L. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Ui\ViewModel;

use Magento\Checkout\Model\Session;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Quote\Model\Quote;

/**
 * Scalapay cart/checkout page data view model
 * @SuppressWarnings(PHPMD.CookieAndSessionMisuse)
 */
class CheckoutPageData implements ArgumentInterface, DataViewModelInterface
{
    /**
     * @var Session
     */
    private $checkoutSession;

    /**
     * @param Session $checkoutSession
     */
    public function __construct(
        Session $checkoutSession
    ) {
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * @return Quote
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    private function getQuote(): Quote
    {
        return $this->checkoutSession->getQuote();
    }

    /**
     * @return float
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function getPrice(): float
    {
        return (float) $this->getQuote()->getGrandTotal();
    }

    /**
     * @return array
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getProductsIds(): array
    {
        $items = $this->getQuote()->getItems();
        if (empty($items)) {
            return [];
        }

        return array_map(static function ($item) {
            return $item->getProductId();
        }, $items);
    }

    /**
     * @return string
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getCountry(): string
    {
        if (!$this->getQuote()->getBillingAddress()) {
            return '';
        }

        return (string) $this->getQuote()->getBillingAddress()->getCountryId();
    }
}
