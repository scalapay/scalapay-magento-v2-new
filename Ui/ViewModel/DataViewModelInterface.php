<?php
/*
 * Copyright © Scalapay S.R.L. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Ui\ViewModel;

/**
 * Widget data view model interface
 * @spi
 */
interface DataViewModelInterface
{
    /**
     * @return float
     */
    public function getPrice(): float;

    /**
     * @return int[]
     */
    public function getProductsIds(): array;

    /**
     * @return string
     */
    public function getCountry(): string;
}
