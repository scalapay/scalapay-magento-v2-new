<?php
/*
 * Copyright © Scalapay S.R.L. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Ui\ViewModel;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Helper\Data as CatalogHelper;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Scalapay\Scalapay\Helper\Config as ConfigHelper;

/**
 * Scalapay product page data view model
 */
class ProductPageData implements ArgumentInterface, DataViewModelInterface
{
    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var CatalogHelper
     */
    private $catalogHelper;

    /**
     * @var ConfigHelper
     */
    private $configHelper;

    /**
     * @param RequestInterface $request
     * @param ProductRepositoryInterface $productRepository
     * @param CatalogHelper $catalogHelper
     * @param ConfigHelper $configHelper
     */
    public function __construct(
        RequestInterface $request,
        ProductRepositoryInterface $productRepository,
        CatalogHelper $catalogHelper,
        ConfigHelper $configHelper
    ) {
        $this->request = $request;
        $this->productRepository = $productRepository;
        $this->catalogHelper = $catalogHelper;
        $this->configHelper = $configHelper;
    }

    /**
     * @return int
     */
    private function getProductId(): int
    {
        if ($this->request->getParam('product_id')) {
            return (int) $this->request->getParam('product_id');
        } else {
            return (int) $this->request->getParam('id');
        }
    }

    /**
     * @return ProductInterface
     * @throws NoSuchEntityException
     */
    private function getProduct(): ProductInterface
    {
        return $this->productRepository->getById($this->getProductId());
    }

    /**
     * @return float
     * @throws NoSuchEntityException
     */
    public function getPrice(): float
    {
        // get the product
        $product = $this->getProduct();

        // return price without including tax if they are not visible on the frontend
        if ($this->configHelper->getTaxConfig('display/type') === 1) {
            return (float) $product->getFinalPrice();
        }

        // return price including tax
        return (float) $this->catalogHelper->getTaxPrice(
            $product,
            $product->getFinalPrice(),
            true
        );
    }

    /**
     * @return array
     */
    public function getProductsIds(): array
    {
        return [$this->getProductId()];
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return '';
    }
}
