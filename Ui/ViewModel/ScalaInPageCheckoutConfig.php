<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Ui\ViewModel;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Scalapay\Scalapay\Helper\Config as ConfigHelper;

/**
 * Class ScalaInPageCheckoutConfig
 *
 * Scalapay in page checkout view model.
 * @author Sclapay Plugin Integration Team
 * @package Scalapay\Scalapay\Ui\ViewModel
 */
class ScalaInPageCheckoutConfig
{
    /** @var ScopeConfigInterface $scopeConfig */
    private $scopeConfig;

    /** @var ConfigHelper $configHelper */
    private $configHelper;

    /**
     * ScalaInPageCheckoutConfig constructor.
     *
     * @param ScopeConfigInterface $scopeConfig
     * @param ConfigHelper $configHelper
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ConfigHelper $configHelper
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->configHelper = $configHelper;
    }

    /**
     * Returns a configuration by a given path.
     *
     * @param string $path
     * @return string
     */
    private function getConfig(string $path): string
    {
        return (string) $this->configHelper->getConfig('scalapay/inpagecheckout/' . $path);
    }

    /**
     * Returns if the in page checkout mode is enabled.
     *
     * @return bool
     */
    public function isEnabled(): bool
    {
        return (int) $this->getConfig('enabled') === 1;
    }

    /**
     * Returns the checkout place order button selector.
     *
     * @return string
     */
    public function getCheckoutPlaceOrderButtonSelector(): string
    {
        return $this->getConfig('checkout_place_order_button_selector');
    }

    /**
     * Returns the checkout required agreement selectors.
     *
     * @return string
     */
    public function getCheckoutRequiredAgreementSelectors(): string
    {
        return $this->getConfig('checkout_required_agreement_selectors');
    }

    /**
     * Returns the Scalapay place order wrapper style.
     *
     * @return string
     */
    public function getScalapayPlaceOrderWrapperStyle(): string
    {
        return $this->getConfig('scalapay_place_order_wrapper_style');
    }
}
