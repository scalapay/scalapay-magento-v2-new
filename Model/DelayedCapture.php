<?php
/*
 * Copyright © Scalapay S.R.L. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Model;

use Magento\Framework\Exception\AuthorizationException;
use Magento\Framework\HTTP\ClientInterface;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Scalapay\Scalapay\Gateway\Settings;
use Scalapay\Scalapay\Model\ResourceModel\GetScalapayOrderToken;
use Scalapay\Scalapay\Model\Merchant\Authorization;
use Scalapay\Scalapay\Helper\Data;

class DelayedCapture
{
    const DELAYED_CAPTURE_STATUS_OK = 'AUTHORIZED';

    /**
     * @var ClientInterface
     */
    private $client;
    /**
     * @var SerializerInterface
     */
    private $serializer;
    /**
     * @var Settings
     */
    private $settings;
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;
    /**
     * @var GetScalapayOrderToken
     */
    private $getScalapayOrderToken;
     /**
     * @var helper
     */
    protected $_helper;

    /**
     * DelayedCapture constructor.
     * @param ClientInterface $client
     * @param SerializerInterface $serializer
     * @param Settings $settings
     * @param OrderRepositoryInterface $orderRepository
     * @param GetScalapayOrderToken $getScalapayOrderToken
     * @param Data $_helper
     */
    public function __construct(
        ClientInterface $client,
        SerializerInterface $serializer,
        Settings $settings,
        OrderRepositoryInterface $orderRepository,
        GetScalapayOrderToken $getScalapayOrderToken,
        Data $helperData
    ) {
        $this->client = $client;
        $this->serializer = $serializer;
        $this->settings = $settings;
        $this->orderRepository = $orderRepository;
        $this->getScalapayOrderToken = $getScalapayOrderToken;
        $this->_helper = $helperData;
    }

    /**
     * @param int $orderId
     * @return void
     * @throws AuthorizationException
     */
    public function execute(int $orderId): void
    {
        $order = $this->orderRepository->get($orderId);
        $orderToken = $this->getScalapayOrderToken->execute($orderId);
        /** @noinspection PhpUndefinedMethodInspection */
        $order->setScalapayOrderToken($orderToken);
        $this->_helper->log('scalapay   delay capture order id: '. $orderId);
        if ($this->settings->getLiveMode()) {
            $this->_helper->log('scalapay live mode delay capture order token: '. $orderToken);
            $key = $this->settings->getProductionApiKey();
            $apiDelayUrl = Authorization::PRODUCTION_URI . 'payments/' . $orderToken . '/delay';
        } else {
            $key = $this->settings->getTestApiKey();
            $this->_helper->log('scalapay test mode delay capture order token: '. $orderToken);
            $apiDelayUrl = Authorization::SANDBOX_URI . 'payments/' . $orderToken . '/delay';
        }

        $this->client->addHeader('Accept', 'application/json');
        $this->client->addHeader('Content-Type', 'application/json');
        $this->client->addHeader('Authorization', 'Bearer ' . $key);
        /** @noinspection PhpUndefinedMethodInspection */
        $this->client->post(
            $apiDelayUrl,
            $this->serializer->serialize(['merchantReference' => $order->getIncrementId()])
        );
        $body = $this->serializer->unserialize($this->client->getBody());
        $this->_helper->log(print_r($body, true));
        if ($body['status'] !== self::DELAYED_CAPTURE_STATUS_OK) {
            throw new AuthorizationException(__($body['message']));
        }
    }
}
