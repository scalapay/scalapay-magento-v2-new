<?php
/*
 * Copyright © Scalapay S.R.L. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Model;

use Magento\Framework\HTTP\ClientInterface;
use Magento\Framework\Serialize\SerializerInterface;
use Scalapay\Scalapay\Gateway\Settings;
use Scalapay\Scalapay\Model\Merchant\Authorization;

class GetPaymentStatus
{
    /**
     * @var ClientInterface
     */
    private $client;
    /**
     * @var Settings
     */
    private $settings;
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * GetPaymentStatus constructor.
     * @param ClientInterface $client
     * @param Settings $settings
     * @param SerializerInterface $serializer
     */
    public function __construct(
        ClientInterface $client,
        Settings $settings,
        SerializerInterface $serializer
    ) {
        $this->client = $client;
        $this->settings = $settings;
        $this->serializer = $serializer;
    }

    /**
     * @param string $orderToken
     * @return array
     */
    public function execute(string $orderToken): array
    {
        if ($this->settings->getLiveMode()) {
            $url = Authorization::PRODUCTION_URI;
            $key = $this->settings->getProductionApiKey();
        } else {
            $url = Authorization::SANDBOX_URI;
            $key = $this->settings->getTestApiKey();
        }
        $this->client->addHeader('Accept', 'application/json');
        $this->client->addHeader('Content-Type', 'application/json');
        $this->client->addHeader('Authorization', 'Bearer ' . $key);
        $this->client->get($url . 'payments/' . $orderToken);

        return $this->serializer->unserialize($this->client->getBody());
    }
}
