<?php
/*
 * Copyright © Scalapay S.R.L. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Model;

use Exception;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\UrlInterface;
use Magento\Quote\Api\Data\CartInterface;
use Scalapay\Scalapay\Factory\Api as ScalapayApi;
use Scalapay\Scalapay\Model\Merchant\OrderToken;
use Scalapay\Scalapay\Model\ResourceModel\SaveScalapayQuoteToken;
use Scalapay\Scalapay\Helper\Data;

class GetRedirectUrl
{
    /**
     * @var GetAuthorization
     */
    private $getAuthorization;
    /**
     * @var ScalapayApi
     */
    private $scalapayApi;
    /**
     * @var GetOrderDetails
     */
    private $getOrderDetails;
    /**
     * @var ManagerInterface
     */
    private $messageManager;
    /**
     * @var UrlInterface
     */
    private $url;
    /**
     * @var SaveScalapayQuoteToken
     */
    private $saveScalapayQuoteToken;
    /**
     * @var helper
     */
    protected $_helper;

    /**
     * GetRedirectUrl constructor.
     * @param GetAuthorization $getAuthorization
     * @param UrlInterface $url
     * @param ScalapayApi $scalapayApi
     * @param GetOrderDetails $getOrderDetails
     * @param ManagerInterface $messageManager
     * @param Data $_helper
     */
    public function __construct(
        GetAuthorization $getAuthorization,
        UrlInterface $url,
        ScalapayApi $scalapayApi,
        GetOrderDetails $getOrderDetails,
        ManagerInterface $messageManager,
        SaveScalapayQuoteToken $saveScalapayQuoteToken,
        Data $helperData
    ) {
        $this->getAuthorization = $getAuthorization;
        $this->scalapayApi = $scalapayApi;
        $this->getOrderDetails = $getOrderDetails;
        $this->messageManager = $messageManager;
        $this->url = $url;
        $this->saveScalapayQuoteToken = $saveScalapayQuoteToken;
        $this->_helper = $helperData;
    }

    /**
     * @param CartInterface $quote
     * @return string
     * @throws NoSuchEntityException
     */
    public function execute(CartInterface $quote): string
    {
        $authorization = $this->getAuthorization->execute();

        $orderDetails = $this->getOrderDetails->execute($quote);

        /** @var OrderToken $orderToken */
        try {
            $orderToken = $this->scalapayApi->createOrder(
                $authorization,
                $orderDetails
            );
            if ($quote->getId() && $orderToken->getToken()) {
                $this->saveScalapayQuoteToken->execute((int)$quote->getId(), $orderToken->getToken());
            }
        } catch (Exception $e) {
            $this->_helper->log('GetRedirectUrl: '. $e->getMessage());
            $this->messageManager->addErrorMessage(__($e->getMessage()));
            return $this->url->getUrl('checkout/cart');
        }

        return $orderToken->getCheckoutUrl();
    }
}
