<?php
/*
 * Copyright © Scalapay S.R.L. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Model;

use Magento\Framework\Exception\AuthorizationException;
use Magento\Framework\HTTP\ClientInterface as HttpClientInterface;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Payment\Gateway\Http\ClientInterface;
use Magento\Payment\Gateway\Http\TransferInterface;
use Scalapay\Scalapay\Gateway\Settings;
use Scalapay\Scalapay\Model\Merchant\Authorization;

class Invalidate implements ClientInterface
{
    /**
     * @var HttpClientInterface
     */
    private $httpClient;
    /**
     * @var SerializerInterface
     */
    private $serializer;
    /**
     * @var Settings
     */
    private $settings;

    /**
     * Invalidate constructor.
     * @param HttpClientInterface $httpClient
     * @param SerializerInterface $serializer
     * @param Settings $settings
     */
    public function __construct(
        HttpClientInterface $httpClient,
        SerializerInterface $serializer,
        Settings $settings
    ) {
        $this->httpClient = $httpClient;
        $this->serializer = $serializer;
        $this->settings = $settings;
    }

    /**
     * @inheritDoc
     * @throws AuthorizationException
     */
    public function placeRequest(TransferInterface $transferObject): array
    {
        if ($this->settings->getLiveMode()) {
            $key = $this->settings->getProductionApiKey();
        } else {
            $key = $this->settings->getTestApiKey();
        }

        /** @var \Magento\Sales\Model\Order $order */
        $order = $transferObject->getBody()['payment']->getOrder();
        /** @noinspection PhpUndefinedMethodInspection */
        $orderToken = $order->getScalapayOrderToken();
        $this->httpClient->addHeader('Accept', 'application/json');
        $this->httpClient->addHeader('Content-Type', 'application/json');
        $this->httpClient->addHeader('Authorization', 'Bearer ' . $key);
        $this->httpClient->post(
            Authorization::SANDBOX_URI . 'payments/' . $orderToken . '/void',
            $this->serializer->serialize(['merchantReference' => $order->getIncrementId()])
        );
        $body = $this->serializer->unserialize($this->httpClient->getBody());

        if (isset($body['message'])) {
            throw new AuthorizationException(__($body['message']));
        }
        $transferObject->getBody()['payment']
            ->setTransactionId($orderToken)
            ->setIsTransactionClosed(true)
            ->setShouldCloseParentTransaction(true);

        return [];
    }
}
