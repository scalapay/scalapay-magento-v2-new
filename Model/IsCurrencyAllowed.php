<?php
/*
 * Copyright © Scalapay S.R.L. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Model;

use Scalapay\Scalapay\Gateway\Settings;

/**
 * Check if currency is allowed
 */
class IsCurrencyAllowed
{
    /**
     * @var Settings
     */
    private $settings;

    /**
     * @param Settings $settings
     */
    public function __construct(Settings $settings)
    {
        $this->settings = $settings;
    }

    /**
     * @param string $currencyCode
     * @return bool
     */
    public function execute(string $currencyCode): bool
    {
        if (!$this->settings->getAllowSpecificCurrencies() || !$currencyCode) {
            return true;
        }

        $allowedCountries = $this->settings->getSpecificCurrencies();
        return in_array($currencyCode, $allowedCountries);
    }
}
