<?php
/*
 * Copyright © Scalapay S.R.L. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Model;

use Scalapay\Scalapay\Gateway\Settings;

/**
 * Check if country is allowed
 */
class IsCountryAllowed
{
    /**
     * @var Settings
     */
    private $settings;

    /**
     * @param Settings $settings
     */
    public function __construct(Settings $settings)
    {
        $this->settings = $settings;
    }

    /**
     * @param string|null $countryCode
     * @return bool
     */
    public function execute(?string $countryCode): bool
    {
        if ($countryCode === null) {
            return true;
        }

        if (!$this->settings->getAllowSpecificCountries() || !$countryCode) {
            return true;
        }

        $allowedCountries = $this->settings->getSpecificCountries();
        return in_array($countryCode, $allowedCountries);
    }
}
