<?php
/*
 * Copyright © Scalapay S.R.L. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Model;

use Magento\Quote\Model\Quote;
use Scalapay\Scalapay\Helper\Data;

class DoubleCheck
{
    /**
     * @var GetPaymentStatus
     */
    private $getPaymentStatus;
    
    protected $_helper;

    /**
     * DoubleCheck constructor.
     * @param GetPaymentStatus $getPaymentStatus
     */
    public function __construct(
        GetPaymentStatus $getPaymentStatus,
        Data $helperData
    ) {
        $this->getPaymentStatus = $getPaymentStatus;
        $this->_helper = $helperData;
    }

    /**
     * @param Quote $quote
     * @return bool
     * @noinspection PhpUndefinedMethodInspection
     */
    public function execute(Quote $quote): bool
    {
		//$this->_helper->log('Scalapay DoubleCheck quote:');
        //$this->_helper->log($quote->getData());
        
        $paymentStatus = $this->getPaymentStatus->execute($quote->getScalapayOrderToken());
        $this->_helper->log('Scalapay DoubleCheck paymentStatus: ');
        $this->_helper->log($paymentStatus);
        if ($quote->getScalapayOrderToken() !== $paymentStatus['token']
            || $quote->getGrandTotal() != $paymentStatus['totalAmount']['amount']
            || $quote->getReservedOrderId() !== $paymentStatus['orderDetails']['merchantReference']
            || $paymentStatus['status'] !== 'authorized'
            || $paymentStatus['captureStatus'] !== 'pending') {
            return false;
        }

        return true;
    }
}
