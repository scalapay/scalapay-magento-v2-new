<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Model\Config;

use Magento\Catalog\Model\Product\Type as ProductType;
use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class ProductTypes
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Model\Config
 */
class ProductTypes implements OptionSourceInterface
{
    /** @var ProductType $productType */
    private $productType;

    /**
     * ProductTypes constructor.
     *
     * @param ProductType $productType
     */
    public function __construct(ProductType $productType)
    {
        $this->productType = $productType;
    }

    /**
     * Returns product types.
     *
     * @return string[]
     */
    public function toOptionArray(): array
    {
        $productTypes = [];
        $productTypes[''] = [
            'value' => '',
            'label' => __('-- No Restrictions --')
        ];
        $productTypeOptions = $this->productType->toOptionArray();
        foreach ($productTypeOptions as $productTypeOption) {
            $productTypes[] = [
                'value' => $productTypeOption['value'],
                'label' => $productTypeOption['label']
            ];
        }
        return $productTypes;
    }
}
