<?php
/*
 * Copyright © Scalapay S.R.L. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Model\Config;

use Magento\Catalog\Model\CategoryFactory;
use Magento\Catalog\Model\ResourceModel\Category\Collection as CategoryCollection;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;
use Magento\Framework\Data\OptionSourceInterface;

class InstallmentsPayin4 implements OptionSourceInterface
{
    /**
     * @inheritDoc
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function toOptionArray(): array
    {
        return [
            ['value' => 4, 'label' => '4']
        ];
    }
}
