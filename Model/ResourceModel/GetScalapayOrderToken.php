<?php
/*
 * Copyright © Scalapay S.R.L. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Model\ResourceModel;

use Magento\Framework\App\ResourceConnection;

class GetScalapayOrderToken
{
    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * GetScalapayOrderToken constructor.
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(
        ResourceConnection $resourceConnection
    ) {
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * @param int $entityId
     * @return string
     */
    public function execute(int $entityId): string
    {
        $connection = $this->resourceConnection->getConnection();

        $select = $connection->select()
            ->from($this->resourceConnection->getTableName('sales_order'), ['scalapay_order_token'])
            ->where('entity_id = ?', $entityId);

        return $connection->fetchOne($select);
    }
}
