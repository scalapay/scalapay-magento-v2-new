<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Model\ResourceModel\PayLater;

use Scalapay\Scalapay\Gateway\SettingsPayLater;
use Scalapay\Scalapay\Helper\Product as ProductHelper;

/**
 * Class IsProductListInDisabledProductTypes
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Model\ResourceModel\PayLater
 */
class IsProductListInDisabledProductTypes
{
    /** @var SettingsPayLater $settingsPayLater */
    private $settingsPayLater;

    /** @var ProductHelper $productHelper */
    private $productHelper;

    /**
     * IsProductListInDisabledProductTypes constructor.
     *
     * @param SettingsPayLater $settingsPayLater
     * @param ProductHelper $productHelper
     */
    public function __construct(
        SettingsPayLater $settingsPayLater,
        ProductHelper $productHelper
    ) {
        $this->settingsPayLater = $settingsPayLater;
        $this->productHelper = $productHelper;
    }

    /**
     * Returns true if there are products with a disabled product type else false.
     *
     * @param int[] $productIds
     * @return bool
     */
    public function execute(array $productIds): bool
    {
        // init vars
        $productsTypes = [];

        // check product ids array
        if (!count($productIds)) {
            return false;
        }

        // check disabled product types array
        $disabledProductTypes = $this->settingsPayLater->getDisabledProductTypes();
        if (!count($disabledProductTypes)) {
            return false;
        }

        // push product types into the specific array
        foreach ($productIds as $productId) {
            $productType = $this->productHelper->getProductType($productId);
            if ($productType && !in_array($productType, $productsTypes)) {
                $productsTypes[] = $productType;
            }
        }

        // return if there are disabled product types
        return !empty(array_intersect($disabledProductTypes, $productsTypes));
    }
}
