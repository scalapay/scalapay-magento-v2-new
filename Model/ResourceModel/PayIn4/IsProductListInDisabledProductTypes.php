<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Model\ResourceModel\PayIn4;

use Scalapay\Scalapay\Gateway\SettingsPayIn4;
use Scalapay\Scalapay\Helper\Product as ProductHelper;

/**
 * Class IsProductListInDisabledProductTypes
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Model\ResourceModel\PayIn4
 */
class IsProductListInDisabledProductTypes
{
    /** @var SettingsPayIn4 $settingsPayIn4 */
    private $settingsPayIn4;

    /** @var ProductHelper $productHelper */
    private $productHelper;

    /**
     * IsProductListInDisabledProductTypes constructor.
     *
     * @param SettingsPayIn4 $settingsPayIn4
     * @param ProductHelper $productHelper
     */
    public function __construct(
        SettingsPayIn4 $settingsPayIn4,
        ProductHelper $productHelper
    ) {
        $this->settingsPayIn4 = $settingsPayIn4;
        $this->productHelper = $productHelper;
    }

    /**
     * Returns true if there are products with a disabled product type else false.
     *
     * @param int[] $productIds
     * @return bool
     */
    public function execute(array $productIds): bool
    {
        // init vars
        $productsTypes = [];

        // check product ids array
        if (!count($productIds)) {
            return false;
        }

        // check disabled product types array
        $disabledProductTypes = $this->settingsPayIn4->getDisabledProductTypes();
        if (!count($disabledProductTypes)) {
            return false;
        }

        // push product types into the specific array
        foreach ($productIds as $productId) {
            $productType = $this->productHelper->getProductType($productId);
            if ($productType && !in_array($productType, $productsTypes)) {
                $productsTypes[] = $productType;
            }
        }

        // return if there are disabled product types
        return !empty(array_intersect($disabledProductTypes, $productsTypes));
    }
}
