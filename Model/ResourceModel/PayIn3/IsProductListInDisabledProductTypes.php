<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Model\ResourceModel\PayIn3;

use Scalapay\Scalapay\Gateway\Settings;
use Scalapay\Scalapay\Helper\Product as ProductHelper;

/**
 * Class IsProductListInDisabledProductTypes
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Model\ResourceModel\PayIn3
 */
class IsProductListInDisabledProductTypes
{
    /** @var Settings $settings */
    private $settings;

    /** @var ProductHelper $productHelper */
    private $productHelper;

    /**
     * IsProductListInDisabledProductTypes constructor.
     *
     * @param Settings $settings
     * @param ProductHelper $productHelper
     */
    public function __construct(
        Settings $settings,
        ProductHelper $productHelper
    ) {
        $this->settings = $settings;
        $this->productHelper = $productHelper;
    }

    /**
     * Returns true if there are products with a disabled product type else false.
     *
     * @param int[] $productIds
     * @return bool
     */
    public function execute(array $productIds): bool
    {
        // init vars
        $productsTypes = [];

        // check product ids array
        if (!count($productIds)) {
            return false;
        }

        // check disabled product types array
        $disabledProductTypes = $this->settings->getDisabledProductTypes();
        if (!count($disabledProductTypes)) {
            return false;
        }

        // push product types into the specific array
        foreach ($productIds as $productId) {
            $productType = $this->productHelper->getProductType($productId);
            if ($productType && !in_array($productType, $productsTypes)) {
                $productsTypes[] = $productType;
            }
        }

        // return if there are disabled product types
        return !empty(array_intersect($disabledProductTypes, $productsTypes));
    }
}
