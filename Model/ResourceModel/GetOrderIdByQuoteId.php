<?php
/*
 * Copyright © Scalapay S.R.L. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Model\ResourceModel;

use Magento\Framework\App\ResourceConnection;

class GetOrderIdByQuoteId
{
    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * GetOrderIdByQuoteId constructor.
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(
        ResourceConnection $resourceConnection
    ) {
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * @param int $quoteId
     * @return int
     */
    public function execute(int $quoteId): int
    {
        $connection = $this->resourceConnection->getConnection();

        $select = $connection->select()
            ->from($this->resourceConnection->getTableName('sales_order'), ['entity_id'])
            ->where('quote_id = ?', $quoteId);

        return (int) $connection->fetchOne($select);
    }
}
