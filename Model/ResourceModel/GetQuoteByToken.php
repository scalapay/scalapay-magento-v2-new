<?php
/*
 * Copyright © Scalapay S.R.L. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Model\ResourceModel;

use Magento\Framework\App\ResourceConnection;

class GetQuoteByToken
{
    /**
     * @var ResourceConnection
     */
    private $resourceConnection;

    /**
     * GetQuoteByReservedOrderId constructor.
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(
        ResourceConnection $resourceConnection
    ) {
        $this->resourceConnection = $resourceConnection;
    }

    /**
     * @param string $orderToken
     * @return int
     */
    public function execute(string $orderToken): int
    {
        $connection = $this->resourceConnection->getConnection();

        $select = $connection->select()
            ->from($this->resourceConnection->getTableName('quote'))
            ->where('scalapay_order_token = ?', $orderToken);

        return (int) $connection->fetchOne($select);
    }
}
