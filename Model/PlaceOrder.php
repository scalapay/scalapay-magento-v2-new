<?php
/*
 * Copyright © Scalapay S.R.L. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Model;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\Exception\AuthorizationException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Quote\Model\QuoteFactory;
use Magento\Sales\Api\Data\OrderInterface;
use Scalapay\Scalapay\Model\ResourceModel\SaveScalapayOrderToken;
use Scalapay\Scalapay\Helper\Data;

/**
 * @SuppressWarnings(PHPMD.CookieAndSessionMisuse)
 */
class PlaceOrder
{
    /**
     * @var CreateOrderAfterCapture
     */
    private $createOrderAfterCapture;
    /**
     * @var SaveScalapayOrderToken
     */
    private $saveScalapayOrderToken;
    /**
     * @var QuoteFactory
     */
    private $quoteFactory;
    /**
     * @var DoubleCheck
     */
    private $doubleCheck;
    
    protected $_helper;

    /**
     * PlaceOrder constructor.
     * @param CreateOrderAfterCapture $createOrderAfterCapture
     * @param SaveScalapayOrderToken $saveScalapayOrderToken
     * @param QuoteFactory $quoteFactory
     * @param DoubleCheck $doubleCheck
     */
    public function __construct(
        CreateOrderAfterCapture $createOrderAfterCapture,
        SaveScalapayOrderToken $saveScalapayOrderToken,
        QuoteFactory $quoteFactory,
        DoubleCheck $doubleCheck,
        Data $helperData
    ) {
        $this->createOrderAfterCapture = $createOrderAfterCapture;
        $this->saveScalapayOrderToken = $saveScalapayOrderToken;
        $this->quoteFactory = $quoteFactory;
        $this->doubleCheck = $doubleCheck;
        $this->_helper = $helperData;
    }

    /**
     * @param CheckoutSession $checkoutSession
     * @param int $quoteId
     * @param string $orderToken
     * @return int
     * @throws LocalizedException
     * @noinspection PhpUndefinedMethodInspection
     */
    public function execute(
        CheckoutSession $checkoutSession,
        int $quoteId,
        string $orderToken
    ): int {
        /** @noinspection PhpDeprecationInspection */
        $quote = $this->quoteFactory->create()->load($quoteId);

        $this->_helper->log('scalapay place order token: '. $orderToken);
        $this->_helper->log('scalapay place quote id: '. $quoteId);
        // Double check transaction validation
        if (!$this->doubleCheck->execute($quote)) {
            //$this->_helper->log($orderToken . ' - ' . $quoteId . ' - Scalapay doubleCheck exception: ');
            //$this->_helper->log($quote->getData());
            throw new AuthorizationException(__('Error on transaction validation (double check).'));
        }
        $checkoutSession->setPlaceOrder(true);
        /** @var OrderInterface|object|null $order */
        $orderId = (int) $this->createOrderAfterCapture->execute(
            $quote
        );
        $this->_helper->log($orderToken . ' - ' . $quoteId . ' - Scalapay PlaceOrder order id: '. $orderId);
        $this->saveScalapayOrderToken->execute($orderId, $orderToken);
        $checkoutSession->unsPlaceOrder();

        return $orderId;
    }
}
