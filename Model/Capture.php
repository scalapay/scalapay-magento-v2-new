<?php
/*
 * Copyright © Scalapay S.R.L. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Model;

use Magento\Framework\Exception\AuthorizationException;
use Magento\Payment\Gateway\Http\ClientInterface;
use Magento\Payment\Gateway\Http\TransferInterface;
use Scalapay\Scalapay\Model\ResourceModel\GetScalapayOrderToken;
use Scalapay\Scalapay\Factory\Api as ScalapayApi;
use Scalapay\Scalapay\Helper\Data;

class Capture implements ClientInterface
{
    /**
     * @var GetAuthorization
     */
    private $getAuthorization;
    /**
     * @var ScalapayApi
     */
    private $scalapayApi;
    /**
     * @var GetScalapayOrderToken
     */
    private $getScalapayOrderToken;
     /**
     * @var helper
     */
    protected $_helper;

    /**
     * Capture constructor.
     * @param GetAuthorization $getAuthorization
     * @param ScalapayApi $scalapayApi
     * @param GetScalapayOrderToken $getScalapayOrderToken
     * @param Data $_helper
     */
    public function __construct(
        GetAuthorization $getAuthorization,
        ScalapayApi $scalapayApi,
        GetScalapayOrderToken $getScalapayOrderToken,
        Data $helperData
    ) {
        $this->getAuthorization = $getAuthorization;
        $this->scalapayApi = $scalapayApi;
        $this->getScalapayOrderToken = $getScalapayOrderToken;
        $this->_helper = $helperData;
    }

    /**
     * @inheritDoc
     * @throws AuthorizationException
     */
    public function placeRequest(TransferInterface $transferObject): array
    {
        /** @var \Magento\Sales\Model\Order $order */
        $order = $transferObject->getBody()['payment']->getOrder();
        $orderToken = $this->getScalapayOrderToken->execute((int) $order->getEntityId());
      
        $this->_helper->log('scalapay capture order token: '. $orderToken);
        /** @noinspection PhpUndefinedMethodInspection */
        $order->setScalapayOrderToken($orderToken);
        $transferObject->getBody()['payment']->setTransactionId($orderToken);
        try {
            $this->scalapayApi->capture(
                $this->getAuthorization->execute(),
                $orderToken
            );
        } catch (\Exception $e) {
            $this->_helper->log('scalapay cature exception: '. $e->getMessage());
            throw new AuthorizationException(__('Payment capture was not authorized from Scalapay.'));
        }

        return [];
    }
}
