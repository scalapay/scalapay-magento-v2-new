<?php
/*
 * Copyright © Scalapay S.R.L. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Model;

use Magento\Catalog\Api\ProductRepositoryInterfaceFactory;
use Magento\Quote\Api\Data\CartInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\ProductMetadataInterface;
use Magento\Framework\Module\ModuleList;
use Scalapay\Scalapay\Helper\Config as ConfigHelper;
use Scalapay\Scalapay\Model\Merchant\Consumer;
use Scalapay\Scalapay\Model\Merchant\Contact;
use Scalapay\Scalapay\Model\Merchant\Discount;
use Scalapay\Scalapay\Model\Merchant\Item;
use Scalapay\Scalapay\Model\Merchant\MerchantOptions;
use Scalapay\Scalapay\Model\Merchant\Money;
use Scalapay\Scalapay\Model\Merchant\OrderDetails;
use Scalapay\Scalapay\Model\Merchant\PluginDetails;

class GetOrderDetails
{
    /**
     * @var ProductRepositoryInterfaceFactory
     */
    private $productRepositoryInterfaceFactory;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var ProductMetadataInterface
     */
    private $productMetadataInterface;

    /**
     * @var ModuleList
     */
    private $moduleList;

    /**
     * @var ConfigHelper
     */
    private $configHelper;

    /**
     * GetOrderDetails constructor.
     *
     * @param ProductRepositoryInterfaceFactory $productRepositoryInterfaceFactory
     * @param StoreManagerInterface $storeManager
     * @param ProductMetadataInterface $productMetadataInterface
     * @param ModuleList $moduleList
     * @param ConfigHelper $configHelper
     */
    public function __construct(
        ProductRepositoryInterfaceFactory $productRepositoryInterfaceFactory,
        StoreManagerInterface $storeManager,
        ProductMetadataInterface $productMetadataInterface,
        ModuleList $moduleList,
        ConfigHelper $configHelper
    ) {
        $this->productRepositoryInterfaceFactory = $productRepositoryInterfaceFactory;
        $this->storeManager = $storeManager;
        $this->productMetadataInterface = $productMetadataInterface;
        $this->moduleList = $moduleList;
        $this->configHelper = $configHelper;
    }

    /**
     * @param CartInterface $quote
     * @return OrderDetails
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(CartInterface $quote): OrderDetails
    {
        //selected payment method
        $paymentMethod = $quote->getPayment()->getMethod();

        //PluginDetails
        $pluginDetails = new PluginDetails();
        $mageVersion = $this->productMetadataInterface->getVersion();
        $scalapayVersion = $this->moduleList->getOne("Scalapay_Scalapay");
        $setupVersion = $scalapayVersion['setup_version'];
        $pluginDetails
            ->setPlatformVersion($mageVersion)
            ->setPluginVersion($setupVersion)
            ->setPlatform("magento")
            ->setCustomized("0");
        
        // Total Amount
        $totalAmount = new Money();
        $totalAmount->setAmount($quote->getGrandTotal());

        // Customer
        $consumer = new Consumer();
        $consumer
            ->setEmail($quote->getBillingAddress()->getEmail())
            ->setGivenNames($quote->getBillingAddress()->getFirstname())
            ->setSurname($quote->getBillingAddress()->getLastname())
            ->setPhoneNumber($quote->getBillingAddress()->getTelephone());

        // Billing Address
        $billingAddress = new Contact();
        /** @noinspection PhpUndefinedMethodInspection */
        $billingAddress
            ->setName($quote->getBillingAddress()->getName())
            ->setLine1($quote->getBillingAddress()->getStreetLine(1))
            ->setLine2($quote->getBillingAddress()->getStreetLine(2))
            ->setPostcode($quote->getBillingAddress()->getPostcode())
            ->setSuburb($quote->getBillingAddress()->getCity())
            ->setCountryCode($quote->getBillingAddress()->getCountryId())
            ->setPhoneNumber($quote->getBillingAddress()->getTelephone());

        // Shippng Address
        $shippingAddress = new Contact();
        $shippingAddress
            ->setName($quote->getShippingAddress()->getName())
            ->setLine1($quote->getShippingAddress()->getStreetLine(1))
            ->setLine2($quote->getShippingAddress()->getStreetLine(2))
            ->setPostcode($quote->getShippingAddress()->getPostcode())
            ->setSuburb($quote->getShippingAddress()->getCity())
            ->setCountryCode($quote->getShippingAddress()->getCountryId())
            ->setPhoneNumber($quote->getShippingAddress()->getTelephone());

        // Items
        $items = [];
        $taxTotal = 0.00;
        /** @var \Magento\Quote\Model\Quote\Item $quoteItem */
        foreach ($quote->getAllVisibleItems() as $quoteItem) {
            $item = new Item();
            $item->setName($quoteItem->getName());
            $item->setSku($quoteItem->getSku());
            $item->setQuantity($quoteItem->getQty());
            $taxTotal += $quoteItem->getTaxAmount();
            $itemPrice = new Money();
            $itemPrice->setAmount((float) $quoteItem->getPrice());
            $item->setPrice($itemPrice);
            $item->setPageUrl($quoteItem->getProduct()->getProductUrl());
            $item->setImageUrl($this->getProductThumbnailUrl($quoteItem->getProduct()->getId()));
            $items[] = $item;
        }

        // Discount
        $discountAmount = new Money();
        $discountAmount->setAmount($quote->getSubtotal() - $quote->getSubtotalWithDiscount());
        $discount = new Discount();
        $discount->setDisplayName('Total discount');
        /** @noinspection PhpParamsInspection */
        $discount->setAmount($discountAmount);
        $discountList[] = $discount;

        // Tax
        $taxAmount = new Money();
        $taxAmount->setAmount($taxTotal);

        // Shipping
        $shippingAmount = new Money();
        $shippingAmount->setAmount($quote->getShippingAddress()->getShippingAmount());

        // Merchant options
        $merchantOptions = new MerchantOptions();
        $confirmUrl = $this->storeManager->getStore()->getUrl('scalapay/confirm');
        $cancelUrl = $this->storeManager->getStore()->getUrl('scalapay/cancel');
        if ($this->configHelper->inPageCheckoutIsEnabled()) {
            $confirmUrl = ConfigHelper::IN_PAGE_CHECKOUT_CDN_HTML .
                '?scalapayPopupOutputRedirect=' .
                base64_encode($confirmUrl);

            $cancelUrl = ConfigHelper::IN_PAGE_CHECKOUT_CDN_HTML .
                '?scalapayPopupOutputRedirect=' .
                base64_encode($cancelUrl);
        }
        $merchantOptions->setRedirectConfirmUrl($confirmUrl);
        $merchantOptions->setRedirectCancelUrl($cancelUrl);

        // Order details
        $orderDetails = new OrderDetails();
        if ($paymentMethod == 'payin4') {
            $frequency = new \Scalapay\Scalapay\Model\Merchant\Frequency();
            $frequency->setFrequencyType('monthly');
            $frequency->setNumber(1);

            /** @noinspection PhpParamsInspection */
            return $orderDetails
                ->setTotalAmount($totalAmount)
                ->setConsumer($consumer)
                ->setBilling($billingAddress)
                ->setShipping($shippingAddress)
                ->setItems($items)
                ->setDiscounts($discountList)
                ->setTaxAmount($taxAmount)
                ->setShippingAmount($shippingAmount)
                ->setMerchant($merchantOptions)
                ->setMerchantReference($quote->getReservedOrderId())
                ->setType('online')
                ->setProduct('pay-in-4')
                ->setFrequency($frequency)
                ->setPluginDetails($pluginDetails);
        } else if ($paymentMethod == 'paylater') {
            $frequency = new \Scalapay\Scalapay\Model\Merchant\Frequency();
            $frequency->setFrequencyType('daily');
            $frequency->setNumber(14);
            /** @noinspection PhpParamsInspection */
            return $orderDetails
                ->setTotalAmount($totalAmount)
                ->setConsumer($consumer)
                ->setBilling($billingAddress)
                ->setShipping($shippingAddress)
                ->setItems($items)
                ->setDiscounts($discountList)
                ->setTaxAmount($taxAmount)
                ->setShippingAmount($shippingAmount)
                ->setMerchant($merchantOptions)
                ->setMerchantReference($quote->getReservedOrderId())
                ->setType('online')
                ->setProduct('later')
                ->setFrequency($frequency)
                ->setPluginDetails($pluginDetails);
        } else {
            $frequency = new \Scalapay\Scalapay\Model\Merchant\Frequency();
            $frequency->setFrequencyType('monthly');
            $frequency->setNumber(1);
            /** @noinspection PhpParamsInspection */
            return $orderDetails
                ->setTotalAmount($totalAmount)
                ->setConsumer($consumer)
                ->setBilling($billingAddress)
                ->setShipping($shippingAddress)
                ->setItems($items)
                ->setDiscounts($discountList)
                ->setTaxAmount($taxAmount)
                ->setShippingAmount($shippingAmount)
                ->setMerchant($merchantOptions)
                ->setMerchantReference($quote->getReservedOrderId())
                ->setType('online')
                ->setProduct('pay-in-3')
                ->setFrequency($frequency)
                ->setPluginDetails($pluginDetails);
        }
    }

    /**
     * Returns the product thumbnail url.
     *
     * @param $productId
     * @return string
     */
    private function getProductThumbnailUrl($productId)
    {
        try {
            $product = $this->productRepositoryInterfaceFactory->create()->getById($productId);
            return $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) .
                'catalog/product' .
                $product->getThumbnail();
        } catch (\Exception $e) {
            return '';
        }
    }
}

