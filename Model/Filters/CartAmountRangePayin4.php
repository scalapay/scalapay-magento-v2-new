<?php
/*
 * Copyright © Scalapay S.R.L. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Model\Filters;

use Magento\Quote\Api\Data\CartInterface;
use Scalapay\Scalapay\Gateway\SettingsPayin4;

class CartAmountRangePayin4 implements FilterInterface
{
    /**
     * @var Settings
     */
    private $settings;

    /**
     * CartAmountRange constructor.
     * @param Settings $settings
     */
    public function __construct(
        SettingsPayin4 $settings
    ) {
        $this->settings = $settings;
    }

    /**
     * @param CartInterface $quote
     * @return bool
     */
    public function execute(CartInterface $quote): bool
    {
        if ($quote->getBaseGrandTotal() < $this->settings->getMinAmount()) {
            return false;
        }
        if ($quote->getBaseGrandTotal() > $this->settings->getMaxAmount()) {
            return false;
        }
        return true;
    }
}
