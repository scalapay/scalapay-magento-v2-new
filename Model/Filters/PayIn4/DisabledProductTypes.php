<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Model\Filters\PayIn4;

use Magento\Quote\Api\Data\CartInterface;
use Scalapay\Scalapay\Model\Filters\FilterInterface;
use Scalapay\Scalapay\Model\ResourceModel\PayIn4\IsProductListInDisabledProductTypes;

/**
 * Class DisabledProductTypes
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Model\Filters\PayIn4
 */
class DisabledProductTypes implements FilterInterface
{
    /** @var IsProductListInDisabledProductTypes $isProductListInDisabledProductTypes */
    private $isProductListInDisabledProductTypes;

    /**
     * DisabledProductTypes constructor.
     *
     * @param IsProductListInDisabledProductTypes $isProductListInDisabledProductTypes
     */
    public function __construct(
        IsProductListInDisabledProductTypes $isProductListInDisabledProductTypes
    ) {
        $this->isProductListInDisabledProductTypes = $isProductListInDisabledProductTypes;
    }

    /**
     * Returns true if the quote has disabled product types else false.
     *
     * @param CartInterface $quote
     * @return bool
     */
    public function execute(CartInterface $quote): bool
    {
        $productIds = array_map(static function ($item) {
            return (int) $item->getProductId();
        }, $quote->getAllItems());

        return !$this->isProductListInDisabledProductTypes->execute($productIds);
    }
}
