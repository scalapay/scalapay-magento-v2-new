<?php
/*
 * Copyright © Scalapay S.R.L. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Model\Filters;

use Magento\Backend\Model\Locale\Resolver;
use Magento\Quote\Api\Data\CartInterface;
use Scalapay\Scalapay\Model\IsLanguageAllowed;

class SpecificLanguages implements FilterInterface
{
    /**
     * @var IsLanguageAllowed
     */
    private $isLanguageAllowed;

    /**
     * @var Resolver
     */
    private $localeResolver;

    /**
     * @param Resolver $localeResolver
     * @param IsLanguageAllowed $isLanguageAllowed
     */
    public function __construct(
        Resolver $localeResolver,
        IsLanguageAllowed $isLanguageAllowed
    ) {
        $this->isLanguageAllowed = $isLanguageAllowed;
        $this->localeResolver = $localeResolver;
    }

    /**
     * @param CartInterface $quote
     * @return bool
     */
    public function execute(CartInterface $quote): bool
    {
        $currentLanguage = $this->localeResolver->getLocale();
        return $this->isLanguageAllowed->execute($currentLanguage);
    }
}
