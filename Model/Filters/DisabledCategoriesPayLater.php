<?php
/*
 * Copyright © Scalapay S.R.L. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Model\Filters;

use Magento\Quote\Api\Data\CartInterface;
use Scalapay\Scalapay\Model\ResourceModel\IsProductListInDisabledCategoriesPayLater;

class DisabledCategoriesPayLater implements FilterInterface
{
    /**
     * @var IsProductListInDisabledCategories
     */
    private $isProductListInDisabledCategories;

    /**
     * @param IsProductListInDisabledCategories $isProductListInDisabledCategories
     */
    public function __construct(
        IsProductListInDisabledCategoriesPayLater $isProductListInDisabledCategories
    ) {
        $this->isProductListInDisabledCategories = $isProductListInDisabledCategories;
    }

    /**
     * @param CartInterface $quote
     * @return bool
     */
    public function execute(CartInterface $quote): bool
    {
        $productIds = array_map(static function ($item) {
            return (int) $item->getProductId();
        }, $quote->getAllItems());

        return !$this->isProductListInDisabledCategories->execute($productIds);
    }
}
