<?php
/*
 * Copyright © Scalapay S.R.L. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Model;

use Magento\Quote\Api\Data\CartInterface;
use Magento\Quote\Model\QuoteManagement;
use Scalapay\Scalapay\Helper\Data;

class CreateOrderAfterCapture
{
    /**
     * @var QuoteManagement
     */
    private $quoteManagement;
    
    protected $_helper;

    /**
     * CreateOrderAfterCapture constructor.
     * @param QuoteManagement $quoteManagement
     */
    public function __construct(
        QuoteManagement $quoteManagement,
        Data $helperData
    ) {
        $this->quoteManagement = $quoteManagement;
        $this->_helper = $helperData;
    }

    /**
     * @param CartInterface $quote
     * @return int
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function execute(CartInterface $quote): int
    {
        $quote->setTotalsCollectedFlag(false);
        $quote->collectTotals();
        //$this->_helper->log(' - Scalapay CreateOrderAfterCapture: ');
        //$this->_helper->log($quote->getData());
        //$this->_helper->log(' - Scalapay CreateOrderAfterCapture: Payment method information ');
        //$data = $quote->getPayment()->getData();
        //$this->_helper->log($data);
        
        return $this->quoteManagement->placeOrder($quote->getId(), $quote->getPayment());
    }
}
