<?php
/**
 * Scalapay_Scalapay
 *
 * Copyright © All rights reserved.
 */
declare(strict_types=1);

namespace Scalapay\Scalapay\Block\InPageCheckout;

use Exception;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\StoreManagerInterface;
use Scalapay\Scalapay\Helper\Config as ConfigHelper;
use Scalapay\Scalapay\Ui\ViewModel\ScalaInPageCheckoutConfig as Config;

/**
 * Class PopupScript
 *
 * @author Scalapay Plugin Integration Team
 * @package Scalapay\Scalapay\Block\InPageCheckout
 */
class PopupScript extends Template
{
    /** @var StoreManagerInterface $storeManagerInterface */
    private $storeManagerInterface;

    /** @var ConfigHelper $configHelper */
    private $configHelper;

    /** @var Config $config */
    private $config;

    /**
     * PopupScript constructor.
     *
     * @param Context $context
     * @param StoreManagerInterface $storeManagerInterface
     * @param ConfigHelper $configHelper
     * @param Config $config
     * @param array $data
     */
    public function __construct(
        Context $context,
        StoreManagerInterface $storeManagerInterface,
        ConfigHelper $configHelper,
        Config $config,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->storeManagerInterface = $storeManagerInterface;
        $this->configHelper = $configHelper;
        $this->config = $config;
    }

    /**
     * Returns the template configuration.
     *
     * @return array
     * @throws NoSuchEntityException
     * @throws Exception
     */
    public function getConfiguration(): array
    {
        // init vars
        $config = [];

        // check if the in page checkout mode is enabled
        if (!$this->config->isEnabled()) {
            return $config;
        }

        // set scalapay popup configurations
        $config['cdnJsUrl'] = ConfigHelper::IN_PAGE_CHECKOUT_CDN_JS;
        $config['cdnHtmlUrl'] = ConfigHelper::IN_PAGE_CHECKOUT_CDN_HTML;
        $config['paymentSelectors'] = $this->getPaymentMethodSelectors(true);
        $config['checkoutPlaceOrderButtonSelector'] = $this->parser($this->config->getCheckoutPlaceOrderButtonSelector());
        $config['checkoutRequiredAgreementSelectors'] = $this->parseCheckoutRequiredAgreementSelectors();
        $config['scalapayPlaceOrderWrapperStyle'] = $this->parser($this->config->getScalapayPlaceOrderWrapperStyle());
        $config['ajaxController'] = $this->storeManagerInterface->getStore()->getUrl('scalapay/redirect/index');
        $config['ajaxMode'] = 'get';
        $config['ajaxControllerPayload'] = json_encode(['ajax' => 1, 'isAjax' => 1]);
        return $config;
    }

    /**
     * Returns Scalapay method selectors JS array content.
     *
     * @param bool $DOMSelector
     * @return string
     */
    private function getPaymentMethodSelectors(bool $DOMSelector): string
    {
        // init vars
        $paymentMethodSelectors = [];

        // add pay in 3 if it is enabled
        if ($this->configHelper->isPaymentEnabled('scalapay/active')) {
            $paymentMethodSelectors[] = $DOMSelector ? "'input[type=\"radio\"]#scalapay'" : "'scalapay'";
        }

        // add pay in 4 if it is enabled
        if ($this->configHelper->isPaymentEnabled('payin4/active')) {
            $paymentMethodSelectors[] = $DOMSelector ? "'input[type=\"radio\"]#payin4'" : "'payin4'";
        }

        // add pay later if it is enabled
        if ($this->configHelper->isPaymentEnabled('paylater/active')) {
            $paymentMethodSelectors[] = $DOMSelector ? "'input[type=\"radio\"]#paylater'" : "'paylater'";
        }
        return implode(', ', $paymentMethodSelectors);
    }

    /**
     * Parses checkout required agreement selectors.
     *
     * @return string
     */
    private function parseCheckoutRequiredAgreementSelectors(): string
    {
        // init vars
        $selectors = [];

        // get checkout required agreement selectors
        $checkoutRequiredAgreementSelectors = $this->config->getCheckoutRequiredAgreementSelectors();

        // exit if there are no selectors
        if (!strlen($checkoutRequiredAgreementSelectors)) {
            return '';
        }

        // explode the string
        $checkoutRequiredAgreementSelectors = explode(',', $checkoutRequiredAgreementSelectors);

        // exit if there are no selectors
        if (!count($checkoutRequiredAgreementSelectors)) {
            return '';
        }

        // parse and return
        foreach ($checkoutRequiredAgreementSelectors as $checkoutRrequiredAgreementSelector) {
            $selectors[] = "'" . $this->parser($checkoutRrequiredAgreementSelector) . "'";
        }
        return implode(',', $selectors);
    }

    /**
     * Parses a param.
     *
     * @param string $param
     * @return string
     */
    private function parser(string $param): string
    {
        // remove spaces at the start and at the end of the string
        $param = trim($param);

        // remove quotes at the start end the end of the string
        $param = preg_replace('~^[\'"]?(.*?)[\'"]?$~', '$1', $param);

        // replace single quotes with double quotes and return
        return str_replace("'", '"', $param);
    }
}
