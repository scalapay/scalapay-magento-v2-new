<?php
/*
 * Copyright © Scalapay S.R.L. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Controller\Redirect;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Message\ManagerInterface;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Model\Quote;
use Scalapay\Scalapay\Model\GetRedirectUrl;
use Scalapay\Scalapay\Gateway\Settings;
use Scalapay\Scalapay\Gateway\SettingsPayin4;
use Scalapay\Scalapay\Gateway\SettingsPayLater;

class Index implements HttpGetActionInterface
{
    /**
     * @var RequestInterface
     */
    private $requestInterface;
    /**
     * @var RedirectFactory
     */
    private $redirectFactory;
    /**
     * @var ResultFactory
     */
    protected $resultFactory;
    /**
     * @var CheckoutSession
     */
    private $checkoutSession;
    /**
     * @var GetRedirectUrl
     */
    private $getRedirectUrl;
    /**
     * @var CartRepositoryInterface
     */
    private $cartRepository;
    /**
     * @var ManagerInterface
     */
    private $messageManager;

    /**
     * Index constructor.
     * @param RequestInterface $requestInterface
     * @param RedirectFactory $redirectFactory
     * @param ResultFactory $resultFactory
     * @param CheckoutSession $checkoutSession
     * @param GetRedirectUrl $getRedirectUrl
     * @param CartRepositoryInterface $cartRepository
     * @param ManagerInterface $messageManager
     */
    public function __construct(
        RequestInterface $requestInterface,
        RedirectFactory $redirectFactory,
        ResultFactory $resultFactory,
        CheckoutSession $checkoutSession,
        GetRedirectUrl $getRedirectUrl,
        CartRepositoryInterface $cartRepository,
        ManagerInterface $messageManager
    ) {
        $this->requestInterface = $requestInterface;
        $this->redirectFactory = $redirectFactory;
        $this->resultFactory = $resultFactory;
        $this->checkoutSession = $checkoutSession;
        $this->getRedirectUrl = $getRedirectUrl;
        $this->cartRepository = $cartRepository;
        $this->messageManager = $messageManager;
    }

    /**
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        /** @var Redirect $redirect */
        $redirect = $this->redirectFactory->create();
        try {
            if ($this->checkoutSession->getQuoteId() === null) {
                if (isset($_SESSION['default']['visitor_data']['quote_id'])) {
                    $this->checkoutSession->setQuoteId($_SESSION['default']['visitor_data']['quote_id']);
                }
            }
            $quote = $this->cartRepository->get((int) $this->checkoutSession->getQuoteId());

            if (!$this->requestInterface->isAjax()) {
                return $redirect->setUrl($this->getRedirectUrl->execute($quote));
            }

            $this->reserveOrderId($quote);
            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $resultJson->setData([
                'redirect' => $this->getRedirectUrl->execute($quote),
                'result' => true,
                'messages' => ''
            ]);
            return $resultJson;
        } catch (NoSuchEntityException $e) {
            if (!$this->requestInterface->isAjax()) {
                $this->messageManager->addErrorMessage(__($e->getMessage()));
                return $redirect->setPath('checkout/cart', ['_secure' => true]);
            }

            $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
            $resultJson->setData([
                'redirect' => '',
                'result' => false,
                'messages' => $e->getMessage()
            ]);
            return $resultJson;
        }
    }

    /**
     * Reserves the order id on the quote.
     *
     * @param Quote $quote
     * @return false|Quote
     */
    private function reserveOrderId(Quote $quote)
    {
        // get scalapay payment method codes
        $scalapayPaymentMethodCodes = [Settings::CODE, SettingsPayin4::CODE, SettingsPayLater::CODE];

        // check conditions
        if ($quote->getReservedOrderId() ||
            !in_array($quote->getPayment()->getMethod(), $scalapayPaymentMethodCodes) ||
            $this->checkoutSession->getPlaceOrder()
        ) {
            return false;
        }

        // reserve the order id
        $quote->reserveOrderId();
        $this->cartRepository->save($quote);
        $this->checkoutSession->setReservedOrderId($quote->getReservedOrderId());
        return $quote;
    }
}
