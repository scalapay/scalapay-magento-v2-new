<?php
/*
 * Copyright © Scalapay S.R.L. All rights reserved.
 * See COPYING.txt for license details.
 */

declare(strict_types=1);

namespace Scalapay\Scalapay\Controller\Confirm;

use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Exception\AuthorizationException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Message\ManagerInterface;
use Magento\Sales\Api\InvoiceOrderInterface;
use Scalapay\Scalapay\Gateway\Settings;
use Scalapay\Scalapay\Model\DelayedCapture;
use Scalapay\Scalapay\Model\PlaceOrder;
use Scalapay\Scalapay\Model\ResourceModel\GetQuoteByReservedOrderId;
use Scalapay\Scalapay\Model\ResourceModel\SaveScalapayQuoteToken;
use Scalapay\Scalapay\Model\ResourceModel\GetQuoteByToken;
use Scalapay\Scalapay\Model\ResourceModel\GetOrderIdByQuoteId;
use Scalapay\Scalapay\Model\ResourceModel\SaveScalapayOrderToken;
use Scalapay\Scalapay\Helper\Data;
use Magento\Sales\Api\OrderRepositoryInterface;
use Scalapay\Scalapay\Gateway\SettingsPayin4;
use Scalapay\Scalapay\Gateway\SettingsPayLater;
use Magento\Sales\Model\Order;

class Index implements HttpGetActionInterface
{
    /**
     * @var RedirectFactory
     */
    private $redirectFactory;
    /**
     * @var RequestInterface
     */
    private $request;
    /**
     * @var ManagerInterface
     */
    private $messageManager;
    /**
     * @var InvoiceOrderInterface
     */
    private $invoiceOrder;
    /**
     * @var Session
     */
    private $checkoutSession;
    /**
     * @var GetQuoteByReservedOrderId
     */
    private $getQuoteByReservedOrderId;
    /**
     * @var SaveScalapayQuoteToken
     */
    private $saveScalapayQuoteToken;
    /**
     * @var Settings
     */
    private $settings;
    /**
     * @var SettingsPayin4
     */
    private $settingsPayin4;
    /**
     * @var SettingsPayLater
     */
    private $settingsPayLater;
    /**
     * @var PlaceOrder
     */
    private $placeOrder;
    /**
     * @var DelayedCapture
     */
    private $delayedCapture;
    /**
     * @var GetQuoteByToken
     */
    private $getQuoteByToken;
    /**
     * @var GetOrderIdByQuoteId
     */
    private $getOrderIdByQuoteId;
    /**
     * @var SaveScalapayOrderToken
     */
    private $saveScalapayOrderToken;
    
    /**
     * @var helper
     */
    protected $_helper;
    
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;
    /**
     * @var order
     */
    protected $order;

    /**
     * Index constructor.
     * @param RedirectFactory $redirectFactory
     * @param RequestInterface $request
     * @param ManagerInterface $messageManager
     * @param InvoiceOrderInterface $invoiceOrder
     * @param Session $checkoutSession
     * @param GetQuoteByReservedOrderId $getQuoteByReservedOrderId
     * @param SaveScalapayQuoteToken $saveScalapayQuoteToken
     * @param Settings $settings
     * @param PlaceOrder $placeOrder
     * @param DelayedCapture $delayedCapture
     * @param GetQuoteByToken $getQuoteByToken
     * @param GetOrderIdByQuoteId $getOrderIdByQuoteId
     * @param SaveScalapayOrderToken $saveScalapayOrderToken
     * @param Data $_helper
     * @param OrderRepositoryInterface $orderRepository
     * @param SettingsPayin4 $settingsPayin4
     * @param SettingsPayLater $settingsPayLater
     * @param Order $order
     */
    public function __construct(
        RedirectFactory $redirectFactory,
        RequestInterface $request,
        ManagerInterface $messageManager,
        InvoiceOrderInterface $invoiceOrder,
        Session $checkoutSession,
        GetQuoteByReservedOrderId $getQuoteByReservedOrderId,
        SaveScalapayQuoteToken $saveScalapayQuoteToken,
        Settings $settings,
        PlaceOrder $placeOrder,
        DelayedCapture $delayedCapture,
        GetQuoteByToken $getQuoteByToken,
        GetOrderIdByQuoteId $getOrderIdByQuoteId,
        SaveScalapayOrderToken $saveScalapayOrderToken,
        Data $helperData,
        OrderRepositoryInterface $orderRepository,
        SettingsPayin4 $settingsPayin4,
        SettingsPayLater $settingsPayLater,
        Order $order
    ) {
        $this->redirectFactory = $redirectFactory;
        $this->request = $request;
        $this->messageManager = $messageManager;
        $this->invoiceOrder = $invoiceOrder;
        $this->checkoutSession = $checkoutSession;
        $this->getQuoteByReservedOrderId = $getQuoteByReservedOrderId;
        $this->saveScalapayQuoteToken = $saveScalapayQuoteToken;
        $this->settings = $settings;
        $this->placeOrder = $placeOrder;
        $this->delayedCapture = $delayedCapture;
        $this->getQuoteByToken = $getQuoteByToken;
        $this->getOrderIdByQuoteId = $getOrderIdByQuoteId;
        $this->saveScalapayOrderToken = $saveScalapayOrderToken;
        $this->_helper = $helperData;
        $this->settingsPayin4 = $settingsPayin4;
        $this->settingsPayLater = $settingsPayLater;
        $this->order = $order;
    }

    /**
     * @return Redirect
     * @noinspection PhpUndefinedMethodInspection
     */
    public function execute(): Redirect
    {
        $status = true;
        $quoteId = null;
        $orderToken = $this->request->getParam('orderToken');

        $this->_helper->log($orderToken . ' - ' . $quoteId . ' - scalapay Confirm Controller orderToken: '. $orderToken);
        $this->_helper->log($orderToken . ' - ' . $quoteId . ' - scalapay Confirm Controller all params: ');
        $this->_helper->log(print_r($this->request->getParams(), true));

        if ($this->checkoutSession->getReservedOrderId()) {
            $quoteId = $this->getQuoteByReservedOrderId->execute($this->checkoutSession->getReservedOrderId());
            $this->_helper->log($orderToken . ' - ' . $quoteId . ' - scalapay Confirm Controller get quote id by getReservedOrderId: '. $quoteId);
            $this->_helper->log($orderToken . ' - ' . $quoteId . ' - scalapay Confirm Controller ReservedOrderId: '. $this->checkoutSession->getReservedOrderId());
        }

        if ($orderToken != "") {
            if ($quoteId == null) {
                $quoteId = $this->getQuoteByToken->execute($orderToken);
                $this->_helper->log($orderToken . ' - ' . $quoteId . ' - scalapay Confirm Controller get quote id by getQuoteByToken: '. $quoteId);
            }
            if ($quoteId == null) {
                $quoteId = (int)$this->request->getParam('quoteid');
                $this->_helper->log($orderToken . ' - ' . $quoteId . ' - scalapay Confirm Controller get quote id by getParam: '. $quoteId);
            }
            $this->saveScalapayQuoteToken->execute($quoteId, $orderToken);
            $this->checkoutSession->unsPlaceOrder();
            try {
                $orderId = $this->placeOrder->execute($this->checkoutSession, $quoteId, $orderToken);
                $this->_helper->log($orderToken . ' - ' . $quoteId . ' - scalapay Confirm Controller get order id by placeOrder: '. $orderId);
                $this->_helper->log("Load order");
                
                //selected payment method
                $order = $this->order->load($orderId);
                $payment = $order->getPayment();
                $paymentMethod = $payment->getMethod();
                $this->_helper->log("Order payment Method: ".$paymentMethod);
                if($paymentMethod == 'payin4'){
                    $this->_helper->log("payin4 order check for delya capture");
                    if ($this->settingsPayin4->getDelayedCapture()) {
                        $this->delayedCapture->execute($orderId);
                    } else {
                        $this->invoiceOrder->execute($orderId, true);
                    }
                    
                } else if ($paymentMethod == 'paylater'){
                    $this->_helper->log("paylater order check for delya capture");
                    if ($this->settingsPayLater->getDelayedCapture()) {
                        $this->delayedCapture->execute($orderId);
                    } else {
                        $this->invoiceOrder->execute($orderId, true);
                    }
                    
                } else {
                    if ($this->settings->getDelayedCapture()) {
                        $this->delayedCapture->execute($orderId);
                    } else {
                        $this->invoiceOrder->execute($orderId, true);
                    }
                }

                
            } catch (Exception | LocalizedException | AuthorizationException | CouldNotSaveException $e) {
                $status = false;
                $orderId = $this->getOrderIdByQuoteId->execute($quoteId);
                if ($orderId) {
                    $status = true;
                    $this->_helper->log($orderToken . ' - ' . $quoteId . ' - scalapay Confirm Controller  exception 2nd try: '. $e->getMessage());
                    $this->saveScalapayOrderToken->execute($orderId, $orderToken);
                    $this->checkoutSession->unsPlaceOrder();
                    $this->_helper->log("Load order");
                     //selected payment method
                     $order = $this->order->load($orderId);
                     $payment = $order->getPayment();
                     $paymentMethod = $payment->getMethod();
                     $this->_helper->log("Order payment Method: ".$paymentMethod);
                     if($paymentMethod == 'payin4'){
                        $this->_helper->log("payin4 order check for delya capture");
                        if ($this->settingsPayin4->getDelayedCapture()) {
                            $this->delayedCapture->execute($orderId);
                        } else {
                            $this->invoiceOrder->execute($orderId, true);
                        }
                    
                    } else if ($paymentMethod == 'paylater'){
                        $this->_helper->log("paylater order check for delya capture");
                        if ($this->settingsPayLater->getDelayedCapture()) {
                            $this->delayedCapture->execute($orderId);
                        } else {
                            $this->invoiceOrder->execute($orderId, true);
                        }
                        
                    } else {
                        if ($this->settings->getDelayedCapture()) {
                            $this->delayedCapture->execute($orderId);
                        } else {
                            $this->invoiceOrder->execute($orderId, true);
                        }
                    }
                    
                } else {
                    $this->_helper->log($orderToken . ' - ' . $quoteId . ' - scalapay Confirm Controller exception: '. $e->getMessage());
                    $this->checkoutSession->unsPlaceOrder();
                    $this->messageManager->addErrorMessage(__($e->getMessage()));
                }
            }
        } else {
            $status = false;
            $this->_helper->log($orderToken . ' - ' . $quoteId . ' - scalapay Confirm Controller error: Order token is empty.');
            $this->checkoutSession->unsPlaceOrder();
            $this->messageManager->addErrorMessage(__('Scalapay order token is not available. '));
        }

        if ($status) {
            $path = 'checkout/onepage/success';
        } else {
            $this->checkoutSession->unsPlaceOrder();
            $path = 'checkout/onepage/failure';
        }

        $this->checkoutSession->unsReservedOrderId();
        /** @var \Magento\Framework\Controller\Result\Redirect $redirect */
        $redirect = $this->redirectFactory->create();
        return $redirect->setPath($path, ['_secure' => true]);
    }
}
